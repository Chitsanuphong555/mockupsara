import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  Table,
  Button
} from "reactstrap";
// react plugin used to create charts
import { Line, Pie } from "react-chartjs-2";
// function that returns a color based on an interval of numbers
// import logo from "./logowomen.png";
// import logogfmis from "./gfmis1.gif";
import "./style.css";

import Stats from "components/Stats/Stats.jsx";

import {
  dashboardEmailStatisticsChart,
  dashboardNASDAQChart
} from "variables/charts.jsx";

import { thead, tbody } from "variables/general";

const buttondashboard = {
    position: 'absolute',
    backgroundColor: 'Transparent',
    backgroundRepeat: 'no-repeat',
    border: 'none',
    cursor:'pointer',
    overflow: 'hidden',
    outline:'none'
}

const dropdownmenu ={
    position: 'absolute',
    top: '60%',
    left: '10%',
    zindexindex: '1000',
    display: 'block',
    float: 'left',
    minwidth: '160px',
    padding: '5px 0',
    margin: '2px 0 0',
    liststyle: 'none',
    fontsize: '14px',
    backgroundColor: 'White',
    border: '1px solid rgba(0,0,0,.15)',
    borderradius: '4px',
    boxshadow: '0 6px 12px rgba(0,0,0,.175)',
    backgroundclip: 'padding-box'
}

class Dashboard extends React.Component {
  constructor(){
    super();
    this.state = {
          displayMenu: false,
        };
   
     this.showDropdownMenu = this.showDropdownMenu.bind(this);
     this.hideDropdownMenu = this.hideDropdownMenu.bind(this);
   
   };
   
   showDropdownMenu(event) {
       event.preventDefault();
       this.setState({ displayMenu: true }, () => {
       document.addEventListener('click', this.hideDropdownMenu);
       });
     }
   
     hideDropdownMenu() {
       this.setState({ displayMenu: false }, () => {
         document.removeEventListener('click', this.hideDropdownMenu);
       });
   
     }

  render() {
    return (
      <div className="content">
        <Row>
          <Col xs={12} sm={6} md={6} lg={2}>
          {/* <div style={ buttondashboard } onClick={this.showDropdownMenu}> */}
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    {/* <div className="icon-big text-center">
                      <img src={logo}  alt="react-logo"/>
                    </div> */}  
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-simple-add text-dark" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">สกส</p>
                      <CardTitle tag="p" className="fontCardtitle">8,225,000</CardTitle>
                    </div>
                  </Col>
                  {/* {this.state.displayMenu ? (
                      <ul style={ dropdownmenu }>
                        <li><a href="#">คลิกดูเงินส่ง[สกส.จังหวัด]</a></li>
                      </ul>
                  ):(null)} */}
                </Row>
              </CardBody>
            </Card>
          {/* </div> */}
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-refresh-69 text-success" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">หมุนเวียน</p>
                      <CardTitle tag="p" className="fontCardtitle">1,000,000</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-money-coins text-danger" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">อุดหนุน</p>
                      <CardTitle tag="p" className="fontCardtitle">2,000,000</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-paper text-primary" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">บริหาร</p>
                      <CardTitle tag="p" className="fontCardtitle">200,000</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-tile-56 text-primary" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">หมวดอื่นๅ</p>
                      <CardTitle tag="p" className="fontCardtitle">25,000</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
          {/* <button style={ buttondashboard }> */}
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-zoom-split text-primary" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">รอตรวจสอบ</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
            {/* </button> */}
          </Col>
        </Row>
        <hr />
        <Row>
          <Col xs={12} sm={6} md={6} lg={2}>
          {/* <button style={ buttondashboard }> */}
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    {/* <div className="icon-big text-center ">
                      <img src={logogfmis} className="img-icon" alt="react-logo" />
                    </div> */}
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-simple-add text-dark" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">GFMIS</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
            {/* </button> */}
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-refresh-69 text-success" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">หมุนเวียน</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-money-coins text-danger" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">อุดหนุน</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-paper text-primary" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">บริหาร</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-tile-56 text-primary" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">หมวดอื่นๆ</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} sm={6} md={6} lg={2}>
          {/* <button style={ buttondashboard } > */}
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col xs={5} md={4}>
                    <div className="icon-big text-center">
                      <i className="nc-icon nc-zoom-split text-primary" />
                    </div>
                  </Col>
                  <Col xs={7} md={8}>
                    <div className="numbers">
                      <p className="card-category">รอตรวจสอบ</p>
                      <CardTitle tag="p" className="fontCardtitle">0</CardTitle>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          {/* </button> */}
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <CardTitle>ข้อมูลปีงบประมาณ</CardTitle>
              </CardHeader>
              <CardBody>
                <Line
                  data={dashboardNASDAQChart.data}
                  options={dashboardNASDAQChart.options}
                  width={400}
                  height={100}
                />
              </CardBody>
            </Card>
          </Col>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <CardTitle tag="h4">รายการรับชำระจาก BILL PAYMENT</CardTitle>
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead className="text-secondary">
                    <tr>
                      {thead.map((prop, key) => {
                        if (key === thead.length - 1)
                          return (
                            <th key={key} className="text-center">
                              {prop}
                            </th>
                          );
                        return <th key={key}>{prop}</th>;
                      })}
                    </tr>
                  </thead>
                  <tbody>
                    {tbody.map((prop, key) => {
                      return (
                        <tr key={key}>
                          {prop.data.map((prop, key) => {
                            if (key === thead.length - 1)
                              return (
                                <td key={key} className="text-center">
                                  {prop}
                                  <Button color="info">ดูรายการตัดชำระเงิน</Button>{' '}
                                </td>
                              );
                            return <td key={key}>{prop}</td>;
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={4}>
            <Card>
              <CardHeader>
                <CardTitle>สถานะโครงการเงินหมุนเวียน</CardTitle>
                <p className="card-category">กราฟแสดงสถานะโครงการเงินหมุนเวียน</p>
              </CardHeader>
              <CardBody>
                <Pie
                  data={dashboardEmailStatisticsChart.data}
                  options={dashboardEmailStatisticsChart.options}
                />
              </CardBody>
              <CardFooter>
                <div className="legend">
                  <i className="fa fa-circle text-primary" />โครงการที่รับชำระครบ(ปิดโครงการ){" "}
                  <i className="fa fa-circle text-danger" /> โครงการรอการชำระ{" "}
                </div>
                <hr />
                <Stats>
                  {[
                    {
                      i: "fas fa-calendar-alt",
                      t: " Number of emails sent"
                    }
                  ]}
                </Stats>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard;
