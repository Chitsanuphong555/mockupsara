import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

// import CardAuthor from "components/CardElements/CardAuthor.jsx";
import FormInputs from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

// import damirBosnjak from "assets/img/damir-bosnjak.jpg";
// import mike from "assets/img/mike.jpg";
// import ayoOgunseinde2 from "assets/img/faces/ayo-ogunseinde-2.jpg";
// import joeGardner2 from "assets/img/faces/joe-gardner-2.jpg";
// import clemOnojeghuo2 from "assets/img/faces/clem-onojeghuo-2.jpg";

class User extends React.Component {
  render() {
    return (
      <div className="content">
        <Row>
          <Col md={12} xs={12}>
            <Card className="card-user">
              <CardHeader>
                <CardTitle>รายละเอียดผู้ใช้งาน</CardTitle>
              </CardHeader>
              <CardBody>
                <form>
                  <FormInputs
                    ncols={["col-md-5 pr-1", "col-md-3 px-1", "col-md-4 pl-1"]}
                    proprieties={[
                      {
                        label: "ชื่อ-นามสกุล",
                        inputProps: {
                          type: "text",
                          disabled: true,
                          defaultValue: "ตัวอย่าง ตัวอย่าง"
                        }
                      },
                      {
                        label: "ตำบล",
                        inputProps: {
                          type: "text",
                          defaultValue: "ในเมือง"
                        }
                      },
                      {
                        label: "อำเภอ",
                        inputProps: {
                          type: "text",
                          placeholder: "เมือง"
                        }
                      }
                    ]}
                  />
                  <FormInputs
                    ncols={["col-md-4 pr-1", "col-md-4 pl-1", "col-md-4 pl-1"]}
                    proprieties={[
                      {
                        label: "จังหวัด",
                        inputProps: {
                          type: "text",
                          placeholder: "ขอนแก่น"
                        }
                      },
                      {
                        label: "อาชีพ",
                        inputProps: {
                          type: "text",
                          placeholder: "แม่บ้าน"
                        }
                      },
                      {
                        label: "เบอร์ติดต่อ",
                        inputProps: {
                          type: "text",
                          placeholder: "099-9999999"
                        }
                      }
                    ]}
                  />
                  <Row>
                    <Col className="text-right">
                      <Button color="success">อนุมัติ</Button>
                      <Button color="danger">ไม่อนุมัติ</Button>
                      <Button color="secondary">กลับ</Button>
                    </Col>
                  </Row>
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default User;
