import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col,
  Table
} from "reactstrap";
import { NavLink } from "react-router-dom";
// import CardAuthor from "components/CardElements/CardAuthor.jsx";
// import FormInputs from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { thead, tbody } from "variables/users";

// import damirBosnjak from "assets/img/damir-bosnjak.jpg";
// import mike from "assets/img/mike.jpg";
// import ayoOgunseinde2 from "assets/img/faces/ayo-ogunseinde-2.jpg";
// import joeGardner2 from "assets/img/faces/joe-gardner-2.jpg";
// import clemOnojeghuo2 from "assets/img/faces/clem-onojeghuo-2.jpg";

class User extends React.Component {
  render() {
    return (
      <div className="content">
        <Row>
          <Col md={12} xs={12}>
            <Card className="card-user">
              <CardHeader>
                <CardTitle>จัดการสมาชิก</CardTitle>
              </CardHeader>
              <CardBody>
                <Button color="primary">ขออนุมัติ</Button>{" "}
                <Button color="success">ผ่านการอนุมัติ</Button>{" "}
                <Button color="danger">ไม่ผ่านการอนุมัติ</Button>{" "}
                <Button color="warning">แก้ไข</Button>{" "}
                <Table responsive>
                  <thead style={{ color: "#34B5B8" }}>
                    <tr>
                      {thead.map((prop, key) => {
                        if (key === thead.length - 1)
                          return (
                            <th key={key} className="text-center">
                              {prop}
                            </th>
                          );
                        return (
                          <th key={key} className="text-center">
                            {prop}
                          </th>
                        );
                      })}
                    </tr>
                  </thead>
                  <tbody>
                    {tbody.map((prop, key) => {
                      return (
                        <tr key={key}>
                          {prop.data.map((prop, key) => {
                            if (key === thead.length - 1) {
                              return (
                                <td key={key} className="text-center">
                                  {prop}
                                </td>
                              );
                            } else if (key === 1) {
                              return (
                                <td key={key} className="text-center">
                                  <NavLink
                                    to={"/userdetail"}
                                    key={tbody[0].data[1]}
                                  >
                                    {prop}
                                  </NavLink>
                                </td>
                              );
                            } else {
                              return (
                                <td key={key} className="text-center">
                                  {prop}
                                </td>
                              );
                            }
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                {/* <Row>
                  <div className="update ml-auto mr-auto">
                    <Button color="primary" round>Update Profile</Button>
                  </div>
                </Row> */}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default User;
