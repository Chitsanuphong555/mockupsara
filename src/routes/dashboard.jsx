import Dashboard from "views/Dashboard/Dashboard.jsx";
import Notifications from "views/Notifications/Notifications.jsx";
import Icons from "views/Icons/Icons.jsx";
import Typography from "views/Typography/Typography.jsx";
import TableList from "views/TableList/TableList.jsx";
// import Maps from "views/Maps/Maps.jsx";
import UserPage from "views/UserPage/UserPage.jsx";
import UserPageDetail from "views/UserPage/UserPageDetail.jsx";

var dashRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    show: true
  },
  {
    path: "/user-page",
    name: "ข้อมูลพนักงาน[DPIS]",
    icon: "nc-icon nc-single-02",
    component: UserPage,
    show: true
  },
  {
    path: "/userdetail",
    name: "รายละเอียดผู้ใช้งาน",
    icon: "nc-icon nc-single-02",
    component: UserPageDetail,
    show: false
  },
  {
    path: "/icons",
    name: "จัดการโครงการหมุนเวียน",
    icon: "nc-icon nc-bell-55",
    component: Icons,
    show: true
  },
  {
    path: "/notifications",
    name: "จัดการโครงการอุดหนุน",
    icon: "nc-icon nc-bell-55",
    component: Notifications,
    show: true
  },
  {
    path: "/tables",
    name: "จัดการงบบริหาร[BPM]",
    icon: "nc-icon nc-single-copy-04",
    component: TableList,
    show: true
  },
  {
    path: "/typography",
    name: "จัดการงบประมาณ[สตรี]",
    icon: "nc-icon nc-single-copy-04",
    component: Typography,
    show: true
  },
  {
    path: "/indicators",
    name: "ข้อมูลตัวชี้วัดปี 2561",
    icon: "nc-icon nc-bell-55",
    component: Typography,
    show: true
  },
  {
    path: "/repay",
    name: "ชำระหนี้/ค่าปรับ/ตัดหนี้/รับคืน",
    icon: "nc-icon nc-money-coins",
    component: Typography,
    show: true
  },
  {
    path: "/members",
    name: "จัดการสมาชิก/ผู้ทรงคุณวุฒิ",
    icon: "nc-icon nc-single-copy-04",
    component: Typography,
    show: true
  },
  {
    path: "/report",
    name: "รายงานกองทุนระดับจังหวัด",
    icon: "nc-icon nc-paper",
    component: Typography,
    show: true
  },
  {
    path: "/videos",
    name: "วิดีโอการใช้งาน",
    icon: "nc-icon nc-button-play",
    component: Typography,
    show: true
  },
  {
    // pro: true,
    path: "/upgrade",
    name: "ออกจากระบบ",
    icon: "nc-icon nc-tap-01",
    show: true
  },
  { redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashRoutes;
